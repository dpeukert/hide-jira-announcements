# Hide Jira Announcements

This extension adds a button to hide a single Jira announcement without hiding all of them.

## Firefox
https://addons.mozilla.org/en-GB/firefox/addon/hide-jira-announcements/

## Chrome
https://chrome.google.com/webstore/detail/idlcilgcedpmhcdbokdbcndnkackkhoh

## License
The contents of this repository are provided under the GPLv3 license or any later version (SPDX identifier: `GPL-3.0-or-later`) with the following attributions:
* The [cyrb53 JS hash function](https://stackoverflow.com/a/52171480) by [bryc](https://stackoverflow.com/users/815680/bryc) released into the public domain
  * part of `src/main.js`

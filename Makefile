.PHONY: package-clean package clean

package-clean:
	rm -rf pkg/ hide-jira-announcements-*.zip

package: package-clean
	cp -r src/ pkg/
	zip -j -r hide-jira-announcements.zip pkg/
	rm -rf pkg/

clean: package-clean

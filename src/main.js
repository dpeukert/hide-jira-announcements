'use strict';

// https://stackoverflow.com/a/52171480 by bryc, public domain
const hash = (str) => {
	let h1 = 0xdeadbeef, h2 = 0x41c6ce57;
	for(let i = 0, ch; i < str.length; i++) {
		ch = str.charCodeAt(i);
		h1 = Math.imul(h1 ^ ch, 2654435761);
		h2 = Math.imul(h2 ^ ch, 1597334677);
	}
	h1 = Math.imul(h1 ^ (h1 >>> 16), 2246822507) ^ Math.imul(h2 ^ (h2 >>> 13), 3266489909);
	h2 = Math.imul(h2 ^ (h2 >>> 16), 2246822507) ^ Math.imul(h1 ^ (h1 >>> 13), 3266489909);
	return 4294967296 * (2097151 & h2) + (h1 >>> 0);
};

const metaName = document.head.querySelector('meta[name="application-name"]');

if(metaName !== null && 'name' in metaName.dataset && metaName.dataset.name === 'jira') {
	const host = location.host;
	let bannerEl = document.querySelector('#announcement-banner');

	if(bannerEl !== null) {
		const bannerHash = hash(bannerEl.innerHTML);

		chrome.storage.sync.get({[host]: []}, (res) => {
			let seenHashes = res[host];

			if(seenHashes.length > 0 && seenHashes.indexOf(bannerHash) !== -1) {
				bannerEl.remove();
			} else {
				bannerEl.style.position = 'relative';

				let button = document.createElement('div');
				button.id = 'hide-jira-announcements-close-button';
				button.innerHTML = 'X';
				button.style.position = 'absolute';
				button.style.top = '2px';
				button.style.right = '8px';
				button.style.padding = '0px 2px';

				button.style.opacity = '.5';
				button.style.borderRadius = '3px';
				button.style.backgroundColor = 'white';
				button.style.border = '1px solid black';
				button.style.cursor = 'pointer';

				button.onmouseenter = () => button.style.opacity = '1';
				button.onmouseleave = () => button.style.opacity = '.5';

				button.addEventListener('click', () => {
					seenHashes.push(bannerHash);
					bannerEl.remove();
					chrome.storage.sync.set({[host]: seenHashes});
				}, false);
				bannerEl.appendChild(button);
			}
		});
	}
}
